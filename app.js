let instantOne =  new Vue({
  el: '#app',
  data: {
    playerHealth: 100,
    monsterHealth: 100,
    gameIsRunning: false,
    turns : []
  },
  methods: {
    startGame: function(){
      this.gameIsRunning = true,
      this.monsterHealth = 100,
      this.playerHealth = 100
    },
    attack: function(params) {
      var demage = this.calculateDemage(3, 12)
      this.monsterHealth -= demage
      this.turns.unshift({
        isPlayer: true,
        text: `player hists for ${demage}`
      })
      if(this.checkWin()){
        return;
      }
      this.monsterAttack()
    },
    specialAttack: function(params) {
      this.monsterHealth -= this.calculateDemage(3, 12)
      if(this.checkWin()){
        return;
      }
      this.monsterAttack()
    },
    heal: function(params) {
      if(this.playerHealth <= 90){
        this.playerHealth += 10
      }else{
        this.playerHealth = 100
      }
    },
    giveUp: function(params) {
      this.gameIsRunning = false
      this.turns = []
    },
    monsterAttack : function(params) {
      var demage = this.calculateDemage(3,10)
      this.playerHealth -= demage
      this.checkWin()
      this.turns.unshift({
        isPlayer: false,
        text: `monster hists for ${demage}`
      })
    },
    calculateDemage: function(min, max) {
      return Math.max(Math.floor(Math.random() * min) + 1, max)
    },
    checkWin: function(params) {
      if(this.playerHealth <= 0){
        if(confirm('you won, new game?')){
          this.startGame()
        }else{
          this.gameIsRunning = false
        }
        return true
      }else{
        if(this.monsterHealth <= 0) {
          if(confirm('you won, new game?')){
            this.startGame()
          }else{
            this.gameIsRunning = false
          }
          return true
        }
      }return false  
    }


  },  
})
